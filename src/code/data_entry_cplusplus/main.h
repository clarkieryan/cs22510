/* 
 * File:   main.h
 * Author: Ryan Clarke (ryc)
 * 
 * Created on 19 March 2012, 20:17
 */

#ifndef MAIN_H
#define	MAIN_H

void getInput();
void addShip();

#endif	/* MAIN_H */

