/* 
 * File:   main.h
 * Author: Ryan Clarke (ryc)
 * 
 * Created on 19 March 2012, 20:17
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
//Include the file control stuff
#include <fcntl.h>
//Import the header file for this file
#include "fileFunctions.h"

using namespace std;

struct flock fl;

fileFunctions(){
    /*Will need two flocks one for locking and one for unlocking*/

    //Sets how much of the document to lock, in this case all of it. 
    fl.l_len =  0;
    //Sets the process to lock the file to. 
    fl.l_pid = getpid();
    //Sets the offset to l_whence
    fl.l_start = 0;
    //Set to F_UNLCK to clear the lock
    fl.l_type = F_RDLCK;
    //Sets the offset of where to lock the file from.
    fl.l_whence = SEEK_SET;
    
    //Opens the file
    /* Will need to create the file first*/
    int fd = open("filename", O_WRONLY);
    
    //Controls the file and set the lock as stated by the flock. 
    fcntl(fd, F_SETLKW, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
    /*F_SETLKW If a file is locked it will wait and then when it gets unlocked
    set the lock to this process use this rather than F_SETLK*/
}


void openFile(string fileName){
    
    //Output is the same as write
    ofstream shipFile;
    shipFile.open (fileName);
    //Need to lock the file? Or is this done automatically
    
    //Example writing to a file
    shipFile << "Writing this to a file.\n";
    shipFile.close();
    
}

void isLocked(){
    
}
