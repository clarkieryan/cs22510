/* 
 * File:   main.cpp
 * Author: Ryan Clarke (ryc)
 *
 * Created on 19 March 2012, 19:37
 */

#include "main.h"
#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */

int main(int argc, char** argv) {
    
    cout << "Welcome to the Saint Georges Channel Navigation Information "
            << "System (SCGNIS) \n"
            << "This part of the software allows you to add a new ship to the"
            << "system."
            << "\nPlease input the ships data \n";
    getInput();
    
    return 0;
}

void getInput(){
    string shipName;
    float longitude, latitude;
    long MMSI, course, speed;
    
    cout << "Please input the MMSI number of the ship \n";
    cin >> MMSI;
    cout << "Please input the ships name \n";
    cin >> shipName;
    cout << "Please input the ships longitude \n";
    cin >> longitude;
    cout << "Please input the ships latitude \n";
    cin >> latitude;
    cout << "Please input the ships course \n";
    cin >> course;
    cout << "Please input the ships speed \n";
    cin >> speed;
    
    cout << "You inputted this" << MMSI << shipName << longitude << latitude
            << course << speed;
    
}

void addShip(long MMSI, string shipName, float longitude, float latitude, long course, long speed){
    //Inside here there will be multiple calls to functions that will check and add the ships folder.
}